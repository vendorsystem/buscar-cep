package com.example.brunoblauzius.teste2.Api;

import com.example.brunoblauzius.teste2.Pojo.CepRequest;
import com.example.brunoblauzius.teste2.Pojo.CepResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by bruno.blauzius on 04/11/2016.
 */
public interface CepApi {

    public final static String URL = "http://agentus.com.br/sistema/";

    public final static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://agentus.com.br/sistema/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @FormUrlEncoded
    @POST("webservices/cep")
    //Call<CepResponse> postCep(@Body CepRequest request);
    Call<CepResponse> postCep(@Field("cep") String request);

}
