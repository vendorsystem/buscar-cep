package com.example.brunoblauzius.teste2;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.brunoblauzius.teste2.Api.CepApi;
import com.example.brunoblauzius.teste2.Pojo.CepResponse;
import com.example.brunoblauzius.teste2.Task.CepTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private Button buscar;
    private EditText cep;
    private CepResponse cepResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buscar = (Button) findViewById(R.id.btn_buscar);
        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscar();
            }
        });


    }


    public void buscar(){
        cep = (EditText) findViewById(R.id.cep);
        /**
         * teste de Asynctask
         */
        cepResponse = new CepResponse();
        cepResponse.setCep( cep.getText().toString() );
        new CepTask2( this ).execute( cepResponse );

    }


    private class CepTask2 extends AsyncTask<Object, Object, CepResponse> {

        CepResponse cep;
        Context context = null;
        ProgressDialog dialog;
        private TextView logradouro;
        private TextView bairro;
        private TextView cidade;
        private TextView estado;


        public CepTask2(Context context){
            this.context = context;
        }


        @Override
        protected CepResponse doInBackground(Object... param) {

            cep = (CepResponse) param[0];

            CepApi cepApi = CepApi.retrofit.create(CepApi.class);
            final Call<CepResponse> call = cepApi.postCep( cep.getCep() );
            call.enqueue(new Callback<CepResponse>() {
                @Override
                public void onResponse(Call<CepResponse> call, Response<CepResponse> response) {
                    int code = response.code();
                    if (code == 200) {

                        cep.setBairro(response.body().getBairro());
                        cep.setCidade(response.body().getCidade());
                        cep.setCep(response.body().getCep());
                        cep.setEstado(response.body().getEstado());

                        logradouro = (TextView) findViewById(R.id.logradouro);
                        cidade = (TextView) findViewById(R.id.cidade);
                        bairro = (TextView) findViewById(R.id.bairro);
                        estado = (TextView) findViewById(R.id.estado);

                        logradouro.setText( response.body().getLogradouro() );
                        cidade.setText( cep.getCidade() );
                        bairro.setText( cep.getBairro() );
                        estado.setText( cep.getEstado() );

                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CepResponse> call, Throwable t) {

                }
            });
            return cep;
        }

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show( context, "Aguarde", "Realizando a consulta...", true, true);
        }

    }

}
