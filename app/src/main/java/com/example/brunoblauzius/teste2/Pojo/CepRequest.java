package com.example.brunoblauzius.teste2.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bruno.blauzius on 04/11/2016.
 */
public class CepRequest implements Serializable {

    @SerializedName("cep")
    public String cep;

    public void setCep(String cep) {
        this.cep = cep;
    }
}
