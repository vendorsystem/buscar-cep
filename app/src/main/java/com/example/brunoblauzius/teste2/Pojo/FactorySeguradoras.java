package com.example.brunoblauzius.teste2.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bruno.blauzius on 19/09/2016.
 */
public class FactorySeguradoras implements Serializable {

    ArrayList<Seguradora> seguradoras;

    public void add(Seguradora seguradora){
        seguradoras.add(seguradoras.size(), seguradora);
    }

    public ArrayList<Seguradora> getSeguradoras(){
        return seguradoras;
    }

    public void set( FactorySeguradoras seguradoras ){
        seguradoras = seguradoras;
    }

    public Integer size(){
        return seguradoras.size();
    }

}
