package com.example.brunoblauzius.teste2.Pojo;

import java.io.Serializable;

/**
 * Created by bruno.blauzius on 19/09/2016.
 */
public class Seguradora implements Serializable{

    private Integer idcompanhias;
    private String companhia;
    private String imagem;
    private Integer codCiaVilla;
    private Integer codCiaMultiCalculo;
    private String conversor;
    private Integer tipoExtrato;
    private String calculaTaxaAdm;
    private Integer tipoExtratoTaxaAdm;
    private String drv;
    private String autorizaEmissao;
    private String autorizaDistResultado;
    private String autorizaBaixaSinistros;
    private String autorizaParcelaPendente;
    private Integer porEsgotamento;
    private Integer geraParcela;
    private String pgLoginAutomatico;
    private String formLoginAutomatico;
    private String status;
    private String autorizaCancelamento;

    public Integer getIdcompanhias() {
        return idcompanhias;
    }

    public void setIdcompanhias(Integer idcompanhias) {
        this.idcompanhias = idcompanhias;
    }

    public String getCompanhia() {
        return companhia;
    }

    public void setCompanhia(String companhia) {
        this.companhia = companhia;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public Integer getCodCiaVilla() {
        return codCiaVilla;
    }

    public void setCodCiaVilla(Integer codCiaVilla) {
        this.codCiaVilla = codCiaVilla;
    }

    public Integer getCodCiaMultiCalculo() {
        return codCiaMultiCalculo;
    }

    public void setCodCiaMultiCalculo(Integer codCiaMultiCalculo) {
        this.codCiaMultiCalculo = codCiaMultiCalculo;
    }

    public String getConversor() {
        return conversor;
    }

    public void setConversor(String conversor) {
        this.conversor = conversor;
    }

    public Integer getTipoExtrato() {
        return tipoExtrato;
    }

    public void setTipoExtrato(Integer tipoExtrato) {
        this.tipoExtrato = tipoExtrato;
    }

    public String getCalculaTaxaAdm() {
        return calculaTaxaAdm;
    }

    public void setCalculaTaxaAdm(String calculaTaxaAdm) {
        this.calculaTaxaAdm = calculaTaxaAdm;
    }

    public Integer getTipoExtratoTaxaAdm() {
        return tipoExtratoTaxaAdm;
    }

    public void setTipoExtratoTaxaAdm(Integer tipoExtratoTaxaAdm) {
        this.tipoExtratoTaxaAdm = tipoExtratoTaxaAdm;
    }

    public String getDrv() {
        return drv;
    }

    public void setDrv(String drv) {
        this.drv = drv;
    }

    public String getAutorizaEmissao() {
        return autorizaEmissao;
    }

    public void setAutorizaEmissao(String autorizaEmissao) {
        this.autorizaEmissao = autorizaEmissao;
    }

    public String getAutorizaDistResultado() {
        return autorizaDistResultado;
    }

    public void setAutorizaDistResultado(String autorizaDistResultado) {
        this.autorizaDistResultado = autorizaDistResultado;
    }

    public String getAutorizaBaixaSinistros() {
        return autorizaBaixaSinistros;
    }

    public void setAutorizaBaixaSinistros(String autorizaBaixaSinistros) {
        this.autorizaBaixaSinistros = autorizaBaixaSinistros;
    }

    public String getAutorizaParcelaPendente() {
        return autorizaParcelaPendente;
    }

    public void setAutorizaParcelaPendente(String autorizaParcelaPendente) {
        this.autorizaParcelaPendente = autorizaParcelaPendente;
    }

    public Integer getPorEsgotamento() {
        return porEsgotamento;
    }

    public void setPorEsgotamento(Integer porEsgotamento) {
        this.porEsgotamento = porEsgotamento;
    }

    public Integer getGeraParcela() {
        return geraParcela;
    }

    public void setGeraParcela(Integer geraParcela) {
        this.geraParcela = geraParcela;
    }

    public String getPgLoginAutomatico() {
        return pgLoginAutomatico;
    }

    public void setPgLoginAutomatico(String pgLoginAutomatico) {
        this.pgLoginAutomatico = pgLoginAutomatico;
    }

    public String getFormLoginAutomatico() {
        return formLoginAutomatico;
    }

    public void setFormLoginAutomatico(String formLoginAutomatico) {
        this.formLoginAutomatico = formLoginAutomatico;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAutorizaCancelamento() {
        return autorizaCancelamento;
    }

    public void setAutorizaCancelamento(String autorizaCancelamento) {
        this.autorizaCancelamento = autorizaCancelamento;
    }

    @Override
    public String toString() {
        return  companhia;
    }
}
