package com.example.brunoblauzius.teste2.Task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brunoblauzius.teste2.Api.CepApi;
import com.example.brunoblauzius.teste2.Pojo.CepResponse;
import com.example.brunoblauzius.teste2.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bruno.blauzius on 17/11/2016.
 */
public class CepTask extends AsyncTask<Object, Object, CepResponse>{

    private final View view;
    CepResponse cep;
    Context context = null;
    ProgressDialog dialog;
    private TextView logradouro;
    private TextView bairro;
    private TextView cidade;
    private TextView estado;


    public CepTask(Context context ,Context base){
        this.context = context;
        view = new View(base);
    }


    @Override
    protected CepResponse doInBackground(Object... param) {

        cep = (CepResponse) param[0];

        CepApi cepApi = CepApi.retrofit.create(CepApi.class);
        final Call<CepResponse> call = cepApi.postCep( cep.getCep() );
        call.enqueue(new Callback<CepResponse>() {
            @Override
            public void onResponse(Call<CepResponse> call, Response<CepResponse> response) {
                int code = response.code();
                if (code == 200) {

                    cep.setBairro(response.body().getBairro());
                    cep.setCidade(response.body().getCidade());
                    cep.setCep(response.body().getCep());
                    cep.setEstado(response.body().getEstado());


                    logradouro = (TextView) view.findViewById(R.id.logradouro);
                    cidade = (TextView) view.findViewById(R.id.cidade);
                    bairro = (TextView) view.findViewById(R.id.bairro);
                    estado = (TextView) view.findViewById(R.id.estado);

                    logradouro.setText( response.body().getLogradouro() );
                    cidade.setText( cep.getCidade() );
                    bairro.setText( cep.getBairro() );
                    estado.setText( cep.getEstado() );

                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CepResponse> call, Throwable t) {

            }
        });
        return cep;
    }

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show( context, "Aguarde", "Realizando a consulta...", true, true);
    }

}
